const
    test = require('ava'),
    { faker } = require('@faker-js/faker'),
    createShortener = require('./main.js'),
    shortener = createShortener({ baseUrl: 'https://d.cv.vg' });

test('Count, list, shorten, get, get info, count again, list again', async t => {
    const
        count = await shortener.getCount(),
        list = await shortener.getList(),
        url = `${faker.internet.url().replace(/^http:/, 'https:')}${faker.system.filePath()}`,
        {
            id,
            creationTimestamp
        } = await shortener.shorten({ url }),
        {
            creationTimestamp: creationTimestamp2,
            config: {
                url: url2
            },
            unmetCondition,
            url: url3,
            randomConfigIndex,
            conditionalConfigIndex
        } = await shortener.get(id),
        {
            creationTimestamp: creationTimestamp3,
            config: {
                url: url4
            },
            isEncrypted,
            deletionTimestamp
        } = await shortener.getInfo(id),
        count2 = await shortener.getCount(),
        list2 = await shortener.getList();
    t.is(typeof count, 'number');
    t.true(Array.isArray(list));
    t.true(list.every(item => typeof item === 'string'));
    t.is(count, list.length);
    t.is(typeof id, 'string');
    t.is(typeof creationTimestamp, 'number');
    t.is(creationTimestamp2, creationTimestamp);
    t.is(url2, url);
    t.is(unmetCondition, undefined);
    t.is(url3, url);
    t.is(randomConfigIndex, undefined);
    t.is(conditionalConfigIndex, undefined);
    t.is(creationTimestamp, creationTimestamp3);
    t.is(url4, url);
    t.is(isEncrypted, false);
    t.is(deletionTimestamp, null);
    t.is(count2, count + 1);
    t.is(count2, list2.length);
    t.true(list2.includes(id));
});